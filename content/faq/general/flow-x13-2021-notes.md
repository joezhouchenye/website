+++
title = "Note for ROG Flow X13 (2021)"
+++

BIOS versions 408 & 409 cannot boot a Linux kernel newer than 5.15.x so you will need to upgrade to the 410 bios [here](https://rog.asus.com/laptops/rog-flow/2021-rog-flow-x13-series/helpdesk_bios).

Supergfxctl v5.0.0+ now supports switching between Hybrid, Integrated and Vfio modes on the Flow X13.