+++
title = "How do I get desktop notifications for asusctl?"
+++

This function is now integrated into the ROG Control Center, so long as you run it in the background you will get the notifications.

You can find all notify settings in the "System Settings" in the ROG Control Center.
