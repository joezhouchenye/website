+++
title = "Why does Nvidia mode give a black screen with Xorg?"
+++

This happens on AMD machines typically, if your machine is AMD then you need to add the following to run on graphical login:

```
xrandr --setprovideroutputsource modesetting NVIDIA-0
xrandr --auto
```

there are many ways to do this so you will need to search for the right way for your login manager.

**KDE/SDDM NOTES** The location of the startup script for KDE/SDDM is wrong on most resources found on the internet: To get the login screen to show up on the laptop screen when in Nvidia mode, the xrandr commands must be added to /etc/sddm/Xsetup (NOT /usr/share/sddm/scripts/Xsetup which most resources mention). (thanks `motoridersd `)